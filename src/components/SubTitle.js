import React from "react";
import SubTitleNode from "./TitleNode";

class SubTitle extends React.Component {
  render() {
    return (
      <div>
        <h2>{this.props.text}</h2>
        <SubTitleNode
          text="Manda mais repetição!"
          feeling={this.props.feeling}
        />
      </div>
    );
  }
}

export default SubTitle;

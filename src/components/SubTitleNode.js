import React from "react";

class SubTitleNode extends React.Component {
  render() {
    return (
      <div>
        {this.props.text} {this.props.feeling}
      </div>
    );
  }
}

export default SubTitleNode;

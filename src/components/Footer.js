import React from "react";
import FooterNode from "./FooterNode";

class Footer extends React.Component {
  render() {
    return (
      <div>
        {this.props.text}
        <FooterNode text="Repetir mais!" feeling={this.props.feeling} />
      </div>
    );
  }
}

export default Footer;

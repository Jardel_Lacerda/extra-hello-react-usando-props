import React from "react";
import "./App.css";

import Title from "./components/Title";
import SubTitle from "./components/SubTitle";
import Body from "./components/Body";
import Footer from "./components/Footer";

class App extends React.Component {
  render() {
    return (
      <header className="App-header">
        <div>
          <Title text="Aprendendo React" feeling="<3" />
          <SubTitle text="Exercício sobre props" feeling="<3" />
          <Body text="Prática leva a perfeição!" feeling="<3" />
          <Footer text="Repetição para praticar!" feeling="<3" />
        </div>
      </header>
    );
  }
}

export default App;

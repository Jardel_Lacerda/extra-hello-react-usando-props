import React from "react";
import TitleNode from "./TitleNode";

class Title extends React.Component {
  render() {
    return (
      <div>
        <h1>{this.props.text}</h1>
        <TitleNode text="Repetir mais ainda!" feeling={this.props.feeling} />
      </div>
    );
  }
}

export default Title;
